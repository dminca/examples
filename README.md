# examples

Example Woodpecker pipeline files (WIP).
Pull requests are accepted/welcome.

## Example pipeline files

| Link | Language | Build System | Comments |
| :--  | :--      | :--          | :--      |
| [C/make.yml](C/make.yml) | C | Make | Simple ci for building a Make based C project |
| [golang/build.yml](golang/build.yml) | golang | golang | Simple ci for building and test a Go project |
| [golang/build-docker.yml](golang/build-docker.yml) | golang | golang / kaniko | CI to build golang project and build various docker container and publish them on DockerHub |
| [Hugo/hugo.yml](Hugo/hugo.yml) | Markdown | Hugo | CI step to build static website files and publish them to Codeberg Pages with Hugo |

# More:
https://codeberg.org/explore/repos?q=woodpecker-ci&topic=1
